use mathru::algebra::linear::matrix::General;

#[test]
fn next_first() {
    let g: General<f64> = General::new(2, 2, vec![1.0, 2.0, 3.0, 4.0]);

    let mut iter = g.column_iter(0);

    assert_eq!(*iter.next().unwrap(), 1.0f64);
    assert_eq!(*iter.next().unwrap(), 2.0f64);
    assert_eq!(iter.next(), None);
}

#[test]
fn next_last() {
    let g: General<f64> = General::new(2, 2, vec![1.0, 2.0, 3.0, 4.0]);

    let mut iter = g.column_iter(1);

    assert_eq!(*iter.next().unwrap(), 3.0f64);
    assert_eq!(*iter.next().unwrap(), 4.0f64);
    assert_eq!(iter.next(), None);
}
