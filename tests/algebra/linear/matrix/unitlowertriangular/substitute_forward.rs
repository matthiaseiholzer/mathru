use mathru::algebra::linear::matrix::General;
use mathru::algebra::linear::matrix::SubstituteForward;
use mathru::algebra::linear::matrix::UnitLowerTriangular;
use mathru::algebra::linear::vector::Vector;

#[test]
fn subst_forward_vector() {
    let a: UnitLowerTriangular<f64> = matrix![1.0, 0.0, 0.0;
                                              5.0, 1.0, 0.0;
                                              3.0, 2.0, 1.0]
    .into();

    let b: Vector<f64> = vector![9.0; 8.0; 7.0];

    let x: Vector<f64> = a.substitute_forward(b.clone()).unwrap();

    assert_relative_eq!(General::from(a) * x, b, epsilon = 1.0e-10);
}

#[test]
fn subst_forward_matrix() {
    let a: UnitLowerTriangular<f64> = matrix![1.0, 0.0, 0.0;
                                              2.0, 1.0, 0.0;
                                              3.0, 5.0, 1.0]
    .into();

    let b: General<f64> = matrix![7.0, 7.0, 7.0; 
                                  8.0, 8.0, 8.0; 
                                  9.0, 9.0, 9.0];

    let x: General<f64> = a.substitute_forward(b.clone()).unwrap();

    assert_relative_eq!(General::from(a) * x, b, epsilon = 1.0e-10);
}
