use crate::mathru::algebra::abstr::Bound;

#[test]
fn lower_bound() {
    assert_eq!(u8::lower_bound(), u8::MIN);
    assert_eq!(u16::lower_bound(), u16::MIN);
    assert_eq!(u32::lower_bound(), u32::MIN);
    assert_eq!(u64::lower_bound(), u64::MIN);
    assert_eq!(u128::lower_bound(), u128::MIN);

    assert_eq!(i8::lower_bound(), i8::MIN);
    assert_eq!(i16::lower_bound(), i16::MIN);
    assert_eq!(i32::lower_bound(), i32::MIN);
    assert_eq!(i64::lower_bound(), i64::MIN);
    assert_eq!(i128::lower_bound(), i128::MIN);

    assert_eq!(f32::lower_bound(), f32::MIN);
    assert_eq!(f64::lower_bound(), f64::MIN);
}

#[test]
fn upper_bound() {
    assert_eq!(u8::upper_bound(), u8::MAX);
    assert_eq!(u16::upper_bound(), u16::MAX);
    assert_eq!(u32::upper_bound(), u32::MAX);
    assert_eq!(u64::upper_bound(), u64::MAX);
    assert_eq!(u128::upper_bound(), u128::MAX);

    assert_eq!(i8::upper_bound(), i8::MAX);
    assert_eq!(i16::upper_bound(), i16::MAX);
    assert_eq!(i32::upper_bound(), i32::MAX);
    assert_eq!(i64::upper_bound(), i64::MAX);
    assert_eq!(i128::upper_bound(), i128::MAX);

    assert_eq!(f32::upper_bound(), f32::MAX);
    assert_eq!(f64::upper_bound(), f64::MAX);
}
