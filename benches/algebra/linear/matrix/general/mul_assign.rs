use criterion::Criterion;
use mathru::algebra::linear::matrix::General;

criterion_group!(
    bench_general_mul_assign,
    mul_assign_matrix, /*faer_mul_assign*/
);

const DIM: usize = 100;

fn mul_assign_matrix(bench: &mut Criterion) {
    bench.bench_function("mul matrix assign", move |bh| {
        bh.iter(|| {
            let mut a: General<f64> = General::new(DIM, DIM, vec![3.0; DIM * DIM]);
            let b: General<f64> = General::new(DIM, DIM, vec![3.0; DIM * DIM]);
            a *= b;
        });
    });
}

// fn faer_mul_assign(bench: &mut Criterion) {
//     bench.bench_function("faer-core mul assign ", move |bh| {
//         use faer_core::Mat;
//         bh.iter(|| {
//             let g1 = Mat::<f64>::from_fn(DIM, DIM, |_, _| 1.0f64);
//             let g2 = g1.clone();

//             let _ = g1 * g2;
//         });
//     });
// }
