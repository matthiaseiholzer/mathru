use std::slice::Iter;

const DIM: usize = 1024;

pub trait ColummIter<T> {
    fn column_iter(&self, column: usize) -> MatrixColumnIterator<T>;
}

impl<T> ColummIter<T> for Vec<T> {
    fn column_iter(&self, column: usize) -> MatrixColumnIterator<T> {
        MatrixColumnIterator::new(self, column)
    }
}

pub struct MatrixColumnIterator<'a, T> {
    iter: Iter<'a, T>,
}

impl<'a, T> MatrixColumnIterator<'a, T> {
    pub fn new(g: &'a Vec<T>, column: usize) -> MatrixColumnIterator<'a, T> {
        let rows = DIM;

        // .iter().skip().take() is slower than [a..b].iter()
        // let iter: Take<Skip<Iter<'_, T>>> = g.data.iter().skip(column * rows).take(rows);
        let first = column * rows;
        let iter = g[first..first + rows].iter();
        MatrixColumnIterator { iter }
    }
}

impl<'a, T> Iterator for MatrixColumnIterator<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}
