use criterion::Criterion;
use mathru::algebra::linear::matrix::General;

criterion_group!(
    bench_general_sub_assign_general,
    general_sub_assign_general,
    vec_sub_assign_vec,
    // faer_sub_assign
);

const DIM: usize = 10024;

fn general_sub_assign_general(bench: &mut Criterion) {
    bench.bench_function("general sub_assign general", move |bh| {
        bh.iter(|| {
            let mut g1 = General::new(DIM, DIM, vec![1.0f64; DIM * DIM]);
            let g2 = g1.clone();
            g1 -= g2;
        });
    });
}

fn vec_sub_assign_vec(bench: &mut Criterion) {
    bench.bench_function("vec sub_assign vec", move |bh| {
        bh.iter(|| {
            let g1 = vec![1.0f64; DIM * DIM];
            let g2 = g1.clone();

            let _ = g1.iter().zip(&g2).map(|(a, b)| a - b).collect::<Vec<f64>>();
        });
    });
}

// fn faer_sub_assign(bench: &mut Criterion) {
//     bench.bench_function("faer-core sub_assign ", move |bh| {
//         use faer_core::Mat;
//         bh.iter(|| {
//             let mut g1 = Mat::<f64>::from_fn(DIM, DIM, |_, _| 1.0f64);
//             let g2 = g1.clone();

//             g1 -= g2;
//         });
//     });
// }
