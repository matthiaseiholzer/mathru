use criterion::Criterion;
use mathru::algebra::linear::matrix::SubstituteForward;
use mathru::algebra::linear::{
    matrix::{General, UnitLowerTriangular},
    vector::Vector,
};

criterion_group!(
    bench_substitute_forward,
    substitute_forward_1024_vector,
    substitute_forward_1024_matrix
);

fn substitute_forward_1024_vector(bench: &mut Criterion) {
    let n = 1024;
    let a: UnitLowerTriangular<f64> = UnitLowerTriangular::new_random(n);

    bench.bench_function("substitute forward 1024 vector", move |bh| {
        bh.iter(|| {
            let y: Vector<f64> = Vector::new_column(General::new_random(n, 1).convert_to_vec());
            let _ = a.substitute_forward(y);
        })
    });
}

fn substitute_forward_1024_matrix(bench: &mut Criterion) {
    let n = 1024;

    let a: UnitLowerTriangular<f64> = UnitLowerTriangular::new_random(n);

    bench.bench_function("substitute forward 1024 matrix", move |bh| {
        bh.iter(|| {
            let y: General<f64> = General::new_random(n, n);
            let _ = a.substitute_forward(y).unwrap();
        })
    });
}
