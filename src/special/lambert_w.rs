//! Lambert W functions.

use crate::algebra::abstr::Real;

/// Provides the principal and secondary branches of the [Lambert W function](https://en.wikipedia.org/wiki/Lambert_W_function).
pub trait LambertW {
    /// The principal branch of the Lambert W function.
    ///
    /// It is the inverse of the function
    ///
    /// ```math
    /// f(w) = we^w
    /// ```
    ///
    /// when w >= -1.
    ///
    /// # Arguments
    ///
    /// * `self` >= 0.0
    ///
    /// # Example
    ///
    /// ```
    /// use mathru::special::lambert_w::LambertW;
    ///
    /// let x = 1.0;
    /// let omega =  x.lambert_w0();
    /// ```
    fn lambert_w0(self) -> Self;

    /// The secondary branch of the Lambert W funciton.
    ///
    /// It is the inverse of
    ///
    /// ```math
    /// f(w) = we^w
    /// ```
    ///
    /// when w < -1.
    ///
    /// # Arguments
    ///
    /// * -1/e <= `self` < 0.0
    ///
    /// # Example
    ///
    /// ```
    /// use mathru::special::lambert_w::LambertW;
    ///
    /// let x = -f64::ln(2.0) / 2.0;
    /// let minus_ln_4 = x.lambert_wm1();
    /// ```
    fn lambert_wm1(self) -> Self;
}

/// These implementations have 50 bits of accuracy.
impl LambertW for f64 {
    #[inline]
    fn lambert_w0(self) -> Self {
        lambert_w::lambert_w0(self)
    }

    #[inline]
    fn lambert_wm1(self) -> Self {
        lambert_w::lambert_wm1(self)
    }
}

impl LambertW for f32 {
    #[inline]
    fn lambert_w0(self) -> Self {
        lambert_w::lambert_w0f(self)
    }

    #[inline]
    fn lambert_wm1(self) -> Self {
        lambert_w::lambert_wm1f(self)
    }
}

/// The principal branch of the Lambert W function.
///
/// It is the inverse of the function
///
/// ```math
/// f(w) = we^w
/// ```
///
/// when w >= -1.
///
/// # Arguments
///
/// * `self` >= 0.0
///
/// # Example
///
/// ```
/// use mathru::special::lambert_w::LambertW;
///
/// let x = 1.0;
/// let omega =  x.lambert_w0();
/// ```
pub fn lambert_w0<T>(x: T) -> T
where
    T: Real + LambertW,
{
    x.lambert_w0()
}

/// The secondary branch of the Lambert W funciton.
///
/// It is the inverse of
///
/// ```math
/// f(w) = we^w
/// ```
///
/// when w < -1.
///
/// # Arguments
///
/// * -1/e <= `self` < 0.0
///
/// # Example
///
/// ```
/// use mathru::special::lambert_w::LambertW;
///
/// let x = -f64::ln(2.0) / 2.0;
/// let minus_ln_4 = x.lambert_wm1();
/// ```
pub fn lambert_wm1<T>(x: T) -> T
where
    T: Real + LambertW,
{
    x.lambert_wm1()
}
