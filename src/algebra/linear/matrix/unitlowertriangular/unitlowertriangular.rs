use crate::algebra::{
    abstr::{Field, Scalar},
    linear::matrix::General,
};
use rand::random;
#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

/// Lower triangular matrix with unit diagonal
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Debug, Clone)]
pub struct UnitLowerTriangular<T> {
    pub(crate) matrix: General<T>,
}

impl<T> UnitLowerTriangular<T>
where
    T: Field + Scalar,
{
    pub fn new(matrix: General<T>) -> UnitLowerTriangular<T> {
        UnitLowerTriangular { matrix }
    }

    pub fn dim(&self) -> (usize, usize) {
        self.matrix.dim()
    }

    pub fn new_random(m: usize) -> UnitLowerTriangular<T> {
        let mut g = General::zero(m, m);

        for i in 0..m {
            for j in i..m {
                g[[i, j]] = if i == j {
                    T::one()
                } else {
                    T::from_f64(random())
                };
            }
        }

        UnitLowerTriangular { matrix: g }
    }
}
