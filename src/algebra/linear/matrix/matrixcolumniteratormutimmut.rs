use super::General;
use std::iter::Zip;
use std::slice::{Iter, IterMut};
pub struct MatrixColumnIteratorMutImmut<'a, T> {
    zip: Zip<IterMut<'a, T>, Iter<'a, T>>,
}

impl<'a, T> MatrixColumnIteratorMutImmut<'a, T> {
    pub fn new(
        g: &'a General<T>,
        mut_column: usize,
        immut_column: usize,
    ) -> MatrixColumnIteratorMutImmut<'a, T> {
        debug_assert_ne!(mut_column, immut_column);
        let rows = g.nrows();

        let first_mut = mut_column * rows;
        let first_immut = immut_column * rows;
        let iter_mut = unsafe {
            let ptr_begin = g.data.as_ptr().offset((first_mut) as isize) as *mut T;
            let slice = std::slice::from_raw_parts_mut(ptr_begin, rows);
            slice.iter_mut()
        };
        let iter = unsafe {
            let ptr_begin = g.data.as_ptr().offset((first_immut) as isize);
            let slice = std::slice::from_raw_parts(ptr_begin, rows);
            slice.iter()
        };
        let zip = iter_mut.zip(iter);
        MatrixColumnIteratorMutImmut { zip: zip }
    }
}

impl<'a, T> Iterator for MatrixColumnIteratorMutImmut<'a, T> {
    type Item = (&'a mut T, &'a T);

    fn next(&mut self) -> Option<Self::Item> {
        self.zip.next()
    }
}
